# split_file_reader

## SplitFileReader

A Rust module to transparently read files that have been split on disk, without combining them.
Implements [`std::io::Read`] and [`std::io::Seek`]

#### Usage
##### Simple Example with Zip Files
List all of the files within a Zip file that has been broken into multiple parts.
```rust
use split_file_reader::SplitFileReader;
use std::path::Path;
use zip::read::ZipArchive;

let sfr = SplitFileReader::new(vec![
    Path::new("./src/tests/files/archives/files.zip.000"),
    Path::new("./src/tests/files/archives/files.zip.001"),
    Path::new("./src/tests/files/archives/files.zip.002"),
])?;

let mut za = ZipArchive::new(sfr)?;
for idx in 0..za.len() {
    println!("{}", za.by_index(idx)?.name());
}
```
These files may be anywhere on disk, or across multiple disks.

SplitFileReader does not support writing.

#### Use case

Many large files are distributed in multiple parts, especially archives.  The general solution
to reassembly is to call `cat` from the terminal, and pipe them into a single cohesive file;
however for various reasons this may not always be possible or desirable.  If the full set of
files is larger than the entire disk; if there is not enough space left to `cat` them all
together; or if only a small set of the payload data is required.
```bash
cat ./files/archives/files.zip.* > ./files/archives/file.zip
```

In these scenarios, using the `SplitFileReader` will provide an alternative solution, enabling
random access throughout the archive without making a single file on disk.

##### Github, Gitlab and Large File Size

Github and Gitlab (as well as other file repositories) impose file size limits.  By parting
these files into sufficiently small chunks, the `SplitFileReader` will be able to make
transparent use of them, as though they were a single cohesive file.  This will remove any
requirements to host these files with pre-fetch or pre-commit scripts, or any other "setup"
mechanism to make use of them.

##### Symmetric Download
Some HTTP file servers set maximum transfer windows.  With the `SplitFileReader`, each piece of
data can be streamed into its own file, and then used directly, without the need to reassemble
them; by piping each file stream directly to disk.  The files will then be immediately
available for use, without a recombination step.

##### Other Uses
Because the file type is transparent to the class, even CSV Files can be split and processed
this way, provided that the column headers are only present on the first file.  The CSV does
not even need to be split along the rows, it can be split at any point (and even mid character
for multi-byte characters).

#### Mechanics
##### File handles
The `SplitFileReader` will make use of only a single File handle at a time.  As the file pointer
moves over file boundaries, a new file handle will be opened before the the existing File handle
is closed.  For functions that regularly seek and read over a file boundary, the File handles
will be opened and closed often; this may not be desirable for every application, and care
should be taken to implement buffers if speed is a concern.

Reading beyond the end of the list of files will cause `read()` to return zero, but will not
close the last File handle.  A `read()` call that crosses the file boundaries will open another,
then close the original, transparently to the calling Rust code, but will always keep at least
one File handle open.  The same applies to `seek()`.

##### Concurrency
The `SplitFileReader` is not designed for concurrent or threaded access, it behaves the same as
any other file that has been opened via [`std::fs::File`] (and in fact uses the
[`std::fs::File`] to operate.)  However, since the data it operates against is read-only,
multiple `SplitFileReader`s can be opened against the same data at the same time.

##### Caveats
While this class can open any arbitrarily split data, Zip chunks that are produced by the `zip`
command are *not* simple binary chunks.  They are logically divided in a separate way.  Zip
files that have been parted via the `split` command, after or during their creation, will work
just fine.

Because the `SplitFileReader` allows random-access to the component files, the `files` list
must also be random-access, indexable, and contain only filepaths.

This library has only been tested with Rust stable, and nightlies are not considered.  Other
than [`std::io`], it has no dependencies for the core library, but has a few light dependencies
for the tests and proof-of-concepts.

