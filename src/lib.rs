/*
    split_file_reader
    Copyright (C) 2021  Xavier Halloran, United States

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! # SplitFileReader
//!
//! A Rust module to transparently read files that have been split on disk, without combining them.
//! Implements [`std::io::Read`] and [`std::io::Seek`]
//!
//! ### Usage
//! #### Simple Example with Zip Files
//! List all of the files within a Zip file that has been broken into multiple parts.
//! ```rust
//! use split_file_reader::SplitFileReader;
//! use std::path::Path;
//! use zip::read::ZipArchive;
//!
//! let sfr = SplitFileReader::new(vec![
//!     Path::new("./src/tests/files/archives/files.zip.000"),
//!     Path::new("./src/tests/files/archives/files.zip.001"),
//!     Path::new("./src/tests/files/archives/files.zip.002"),
//! ])?;
//!
//! let mut za = ZipArchive::new(sfr)?;
//! for idx in 0..za.len() {
//!     println!("{}", za.by_index(idx)?.name());
//! }
//! ```
//! These files may be anywhere on disk, or across multiple disks.
//!
//! SplitFileReader does not support writing.
//!
//! Because the SplitFileReader impelments [`std::io::Read`] and [`std::io::Seek`], this may
//! be passed anywhere else a [`std::fs::File`] is expected, such as [`zip::read::ZipArchive`]
//! or [`tar::Archive`].
//!
//! ### Use case
//!
//! Many large files are distributed in multiple parts, especially archives.  The general solution
//! to reassembly is to call `cat` from the terminal, and pipe them into a single cohesive file;
//! however for various reasons this may not always be possible or desirable.  If the full set of
//! files is larger than the entire disk; if there is not enough space left to `cat` them all
//! together; or if only a small set of the payload data is required.
//! ```bash
//! cat ./files/archives/files.zip.* > ./files/archives/file.zip
//! ```
//!
//! In these scenarios, using the `SplitFileReader` will provide an alternative solution, enabling
//! random access throughout the archive without making a single file on disk.
//!
//! #### Github, Gitlab and Large File Size
//!
//! Github and Gitlab (as well as other file repositories) impose file size limits.  By parting
//! these files into sufficiently small chunks, the `SplitFileReader` will be able to make
//! transparent use of them, as though they were a single cohesive file.  This will remove any
//! requirements to host these files with pre-fetch or pre-commit scripts, or any other "setup"
//! mechanism to make use of them.
//!
//! #### Symmetric Download
//! Some HTTP file servers set maximum transfer windows.  With the `SplitFileReader`, each piece of
//! data can be streamed into its own file, and then used directly, without the need to reassemble
//! them; by piping each file stream directly to disk.  The files will then be immediately
//! available for use, without a recombination step.
//!
//! #### Other Uses
//! Because the file type is transparent to the class, even CSV Files can be split and processed
//! this way, provided that the column headers are only present on the first file.  The CSV does
//! not even need to be split along the rows, it can be split at any point (and even mid character
//! for multi-byte characters).
//!
//! ### Mechanics
//! #### File handles
//! The `SplitFileReader` will make use of only a single File handle at a time.  As the file pointer
//! moves over file boundaries, a new file handle will be opened before the the existing File handle
//! is closed.  For functions that regularly seek and read over a file boundary, the File handles
//! will be opened and closed often; this may not be desirable for every application, and care
//! should be taken to implement buffers if speed is a concern.
//!
//! Reading beyond the end of the list of files will cause `read()` to return zero, but will not
//! close the last File handle.  A `read()` call that crosses the file boundaries will open another,
//! then close the original, transparently to the calling Rust code, but will always keep at least
//! one File handle open.  The same applies to `seek()`.
//!
//! #### Concurrency
//! The `SplitFileReader` is not designed for concurrent or threaded access, it behaves the same as
//! any other file that has been opened via [`std::fs::File`] (and in fact uses the
//! [`std::fs::File`] to operate.)  However, since the data it operates against is read-only,
//! multiple `SplitFileReader`s can be opened against the same data at the same time.
//!
//! #### Caveats
//! While this class can open any arbitrarily split data, Zip chunks that are produced by the `zip`
//! command are *not* simple binary chunks.  They are logically divided in a separate way.  Zip
//! files that have been parted via the `split` command, after or during their creation, will work
//! just fine.
//!
//! Because the `SplitFileReader` allows random-access to the component files, the `files` list
//! must also be random-access, indexable, and contain only filepaths.
//!
//! This library has only been tested with Rust stable, and nightlies are not considered.  Other
//! than [`std::io`], it has no dependencies for the core library, but has a few light dependencies
//! for the tests and proof-of-concepts.
//!

/*
mem::discriminant(&Kestrer):
- You should prefer multiple methods over a single method that takes an enum and just does a big match (like advance_file_desc).
      The lines become a bit blurred when the method does stuff outside the match or there are several methods that take a Direction enum, but here it's fairly clear.
      This has the advantage of greater type-accuracy and safety - Direction::Stationary can't return false so advance_file_desc_stationary (you should probably shorten that) doesn't even have to return a bool!

- Instead of new taking a Vec<P> it could take an impl IntoIterator<Item = P>, which you then later collect into a Vec with .into_iter().collect(). This allows it to be more flexible, e.g. you can pass in &["file1", "file2"]
 */
#[cfg(test)]
mod tests;

use std::convert::TryFrom;
use std::fs::File;
use std::io;
use std::io::{ErrorKind, Read, Seek, SeekFrom};
use std::path::Path;

enum Direction {
    Forward,
    Head,
    Backward,
}

///
/// Should act transparently as an [`std::io::File`] that has been split into many parts.
///
/// Acts file-like for a list of files opened read-only in binary mode.
/// Does not support writing.  Implements [`std::io::Read`] and [`std::io::Seek`]
///
#[derive(Debug)]
pub struct SplitFileReader<P> {
    file_names: Vec<P>,
    current_fh: Option<File>,
    current_file_idx: usize,
    told: u64,
}

impl<P: AsRef<Path>> SplitFileReader<P> {
    ///
    /// Supply a list of Files to browse.  This order matters, but not uniqueness.  Entities need
    /// not be unique or exist on the same disk.
    ///
    /// Because the Split File Reader impelments [`std::io::Read`] and [`std::io::Seek`], this may
    /// be passed anywhere else a [`std::fs::File`] is expected, such as
    /// [`zip::read::ZipArchive::new`] or [`tar::Archive::new`].
    ///
    /// Opens first file immediately, same as a call to [`std::fs::File`]
    ///
    /// # Errors
    ///
    /// This function may produce an error if the underlying [`std::fs::File::open`] call produces
    /// an error.
    ///
    /// # Example
    ///
    /// ```
    /// use split_file_reader::SplitFileReader;
    /// use std::path::Path;
    /// let mut sfr = SplitFileReader::new(vec![Path::new(
    ///     "./src/tests/files/plaintext/Adventures_In_Wonderland.txt",
    /// )])?;
    /// ```
    ///
    pub fn new(file_names: Vec<P>) -> io::Result<Self> {
        // Create a new Split File Reader.
        // Opens a new file, just like io::File() would do, so it could produce an error.
        let mut s = Self {
            file_names,
            current_fh: None,
            current_file_idx: 0,
            told: 0,
        };
        s.advance_file_handle(Direction::Head)?;
        Ok(s)
    }

    ///
    /// Will never advance off end of list.
    /// Returns True if successfully opened new file.
    /// Returns False if no more files in list (logical EOF or Head).
    /// Error if cannot open expected file for some reason, or other underlying IOError.
    ///
    /// Takes no control over the `tell` values, that is managed elsewhere, via
    /// the `scan` and `seek` functions.
    ///
    /// This is the only function that should alter the file handles.
    ///
    /// # Errors
    ///
    /// This function may produce an error if the underlying [`std::fs::File::seek`] call produces
    /// an error.
    /// Because this seek is capable of traversing the Split File file boundaries, it may produce an
    /// error if the underlying [`std::fs::File::open`] call produces an error.
    ///
    /// TODO: Might be better as a custom iter or generator.
    ///
    fn advance_file_handle(&mut self, direction: Direction) -> io::Result<bool> {
        fn attempt_open<P: AsRef<Path>>(filepath: &P) -> io::Result<File> {
            let f = File::open(filepath);
            match f {
                Ok(f) => Ok(f),
                Err(e) => Err(io::Error::new(
                    e.kind(),
                    format!(
                        "`{}` couldn't be opened: {}",
                        filepath.as_ref().display(),
                        e
                    ),
                )),
            }
        }

        match direction {
            // This is the "default" case, called when reads advance over a file boundary.
            Direction::Forward => {
                if self.current_file_idx + 1 >= self.file_names.len() {
                    Ok(false)
                } else {
                    self.current_file_idx += 1;
                    let filepath = &self.file_names[self.current_file_idx];
                    self.current_fh = Some(attempt_open(filepath)?);
                    self.current_fh.as_ref().unwrap().seek(SeekFrom::Start(0))?;
                    Ok(true)
                }
            }
            // Used for first time opening the files, or seeking to the head.
            // This is a shortcut mechanism for `scan_backwards` to avoid opening all the
            // intermediary files from current to the start.
            Direction::Head => {
                self.current_file_idx = 0;
                let filepath = &self.file_names[self.current_file_idx];
                self.current_fh = Some(attempt_open(filepath)?);
                self.current_fh.as_ref().unwrap().seek(SeekFrom::Start(0))?;
                Ok(true)
            }
            Direction::Backward => {
                if self.current_file_idx <= 0 {
                    Ok(false)
                } else {
                    self.current_file_idx -= 1;
                    let filepath = &self.file_names[self.current_file_idx];
                    self.current_fh = Some(attempt_open(filepath)?);
                    self.current_fh.as_ref().unwrap().seek(SeekFrom::End(0))?;
                    Ok(true)
                }
            }
        }
    }

    ///
    /// Shortcut operation for `seek(SeekFrom::Start(0))` and other mathematically-identical
    /// operations.  Traversing the list can be expensive for IO operations, so this bypasses them
    /// if movement is going to a known special-case location.
    ///
    fn seek_to_head(&mut self) -> io::Result<()> {
        self.advance_file_handle(Direction::Head)?;
        self.told = 0;
        Ok(())
    }

    ///
    /// Seeking from `SeekFrom::End(X))` is tricky here, unlike a single file where the end location
    /// is known, each file needs to be opened, its size checked and counted, then the next file
    /// needs to be opened.
    ///
    fn seek_to_tail(&mut self) -> io::Result<()> {
        loop {
            let mut cfh = self.current_fh.as_ref().ok_or(io::Error::new(
                io::ErrorKind::NotConnected,
                "No open file handle",
            ))?;
            let start = cfh.seek(SeekFrom::Current(0))?;
            let end = cfh.seek(SeekFrom::End(0))?;
            self.told += end - start;
            // Attempt to open the next file if possible.
            // True if advanced, False if at end of list, Err if cannot open file.
            let advanced = self.advance_file_handle(Direction::Forward)?;
            // Safely advanced nowhere.
            if !advanced {
                break;
            }
        }
        Ok(())
    }

    ///
    /// Move through the list of Files, one at a time, until the desired offset is located.
    ///
    fn scan_forward(&mut self, offset: i64) -> io::Result<u64> {
        let mut remaining = offset;
        while remaining > 0 {
            let mut cfh = self.current_fh.as_ref().ok_or(io::Error::new(
                io::ErrorKind::NotConnected,
                "No open file handle",
            ))?;
            let start_pos = cfh.seek(SeekFrom::Current(0))?;
            let mut end_pos = cfh.seek(SeekFrom::End(0))?;
            let mut moved = (end_pos - start_pos) as i64;
            // Overshot
            if moved > remaining {
                let corrective_move = remaining - moved;
                end_pos = cfh.seek(SeekFrom::Current(corrective_move))?;
                moved = (end_pos - start_pos) as i64;
                self.told += moved as u64;
                remaining = 0;
            // More to go.
            } else {
                remaining -= moved;
                self.told += moved as u64;
                if !self.advance_file_handle(Direction::Forward)? {
                    break;
                }
            }
        }
        Ok(self.told)
    }

    ///
    /// Move through the list of Files, one at a time, until the desired offset is located.
    ///
    fn scan_backward(&mut self, offset: i64) -> io::Result<u64> {
        fn err_for_bad_offset(new_told: i64) -> io::Result<u64> {
            if new_told < 0 {
                return Err(io::Error::new(
                    ErrorKind::InvalidInput,
                    "Bad offset for seek",
                ));
            }
            Ok(new_told as u64)
        }
        let mut remaining = offset;
        while remaining < 0 {
            let mut cfh = self.current_fh.as_ref().ok_or(io::Error::new(
                io::ErrorKind::NotConnected,
                "No open file handle",
            ))?;
            let start_pos = cfh.seek(SeekFrom::Current(0))? as i64;
            let mut end_pos = cfh.seek(SeekFrom::Start(0))? as i64;
            let mut moved = end_pos - start_pos;
            // Overshot.
            if moved < remaining {
                let corrective_move = -(moved - remaining);
                end_pos = cfh.seek(SeekFrom::Current(corrective_move))? as i64;
                moved = (end_pos - start_pos) as i64;
                self.told = err_for_bad_offset(self.told as i64 + moved)?;
                remaining = 0;
            // More to go.
            } else {
                remaining -= moved;
                self.told = err_for_bad_offset(self.told as i64 + moved)?;
                if !self.advance_file_handle(Direction::Backward)? {
                    break;
                }
            }
        }
        Ok(self.told)
    }
    ///
    /// Convenience function, logically identical to a seek to nowhere to get the current offset.
    ///
    pub fn tell(&mut self) -> io::Result<u64> {
        Ok(self.told)
    }
}

impl<P: AsRef<Path>> Seek for SplitFileReader<P> {
    ///
    /// Moves the current offset as specified.
    ///
    /// Returns the current offset, in bytes.
    ///
    /// Seek to end of stream will iterate the list of Files, opening each one in turn, seeking to
    /// the Head, then to the Tail, counting total number of bytes for each file, until the file
    /// list is exhausted.  From there, the list will be iterated backwards to the offset.
    ///
    /// # Errors
    ///
    /// This function may produce an error if the underlying [`std::fs::File::seek`] call produces
    /// an error.
    /// Because this seek is capable of traversing the Split File file boundaries, it may produce an
    /// error if the underlying [`std::fs::File::open`] call produces an error.
    ///
    /// # Example
    ///
    /// ```
    /// use split_file_reader::SplitFileReader;
    /// use std::path::Path;
    /// use std::io::{Seek, SeekFrom};
    /// let mut sfr = SplitFileReader::new(vec![Path::new(
    ///     "./src/tests/files/plaintext/Adventures_In_Wonderland.txt",
    /// )])?;
    /// let current_offset = sfr.seek(SeekFrom::Current(5))?;
    /// ```
    ///
    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
        let how_far_to_go: i64 = match pos {
            SeekFrom::Start(offset) => {
                if offset == 0 {
                    // Shortcut seek to head.
                    self.seek_to_head()?;
                    0
                } else {
                    -(self.told as i64 - i64::try_from(offset).unwrap()) as i64
                }
            }
            SeekFrom::Current(offset) => {
                if offset + self.told as i64 == 0 {
                    // Shortcut seek to head.
                    self.seek_to_head()?;
                    0
                } else {
                    offset
                }
            }
            SeekFrom::End(offset) => {
                self.seek_to_tail()?;
                if offset + self.told as i64 == 0 {
                    // Shortcut seek to head.
                    self.seek_to_head()?;
                    0
                } else {
                    offset
                }
            }
        };
        if how_far_to_go > 0 {
            self.scan_forward(how_far_to_go)
        } else if how_far_to_go < 0 {
            self.scan_backward(how_far_to_go)
        } else {
            // Nowhere to go.
            Ok(self.told)
        }
    }
}

impl<P: AsRef<Path>> Read for SplitFileReader<P> {
    ///
    /// Pull some bytes from this source into the specified buffer, returning how many bytes were
    /// read.
    ///
    /// This function blocks waiting for data.
    ///
    /// The buffer is passed directly to [`std::fs::File::read`], and may be sliced and applied to
    /// `read` against multiple [`std::fs::File`]s repeatedly.  There may be zero or more total
    /// system calls against zero or more total files.
    ///
    /// If the return value of this method is Ok(n), then it is guaranteed that 0 <= n <= buf.len().
    /// A nonzero n value indicates that the buffer buf has been filled in with n bytes of data from
    /// this source. If n is 0, then it can indicate one of two scenarios:
    ///
    /// 1. This reader has reached its "end of file" and will no longer be able to produce bytes.
    /// 2. The buffer specified was 0 bytes in length.
    ///
    /// It is not an error if the returned value n is smaller than the buffer size, even when the
    /// reader is not at the end of the stream yet. This may happen for example because fewer bytes
    /// are actually available right now (e. g. being close to end-of-file) or because read() was
    /// interrupted by a signal.
    ///
    /// Because the buffer is passed directly to the [`std::fs::File::read`] function, there is no
    /// assertion regarding the input integrity of the buffer, except for whatever
    /// [`std::fs::File::read`] offers.
    ///
    /// # Errors
    ///
    /// This function may produce an error if the underlying [`std::fs::File::read`] call produces
    /// an error.
    /// Because this seek is capable of traversing the Split File file boundaries, it may produce an
    /// error if the underlying [`std::fs::File::open`] call produces an error.
    ///
    /// # Example
    ///
    /// ```
    /// use split_file_reader::SplitFileReader;
    /// use std::path::Path;
    /// use std::io::Read;
    /// let mut sfr = SplitFileReader::new(vec![Path::new(
    ///     "./src/tests/files/plaintext/Adventures_In_Wonderland.txt",
    /// )])?;
    /// let mut buffer = [0; 16];
    /// let read_amt = sfr.read(& mut buffer)?;
    /// ```
    ///
    ///
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut read_sofar: usize = 0;
        loop {
            let mut cfh = self.current_fh.as_ref().ok_or(io::Error::new(
                io::ErrorKind::NotConnected,
                "No open file handle",
            ))?;
            let read_amt = cfh.read(&mut buf[read_sofar..])?;
            // Successful read from disk.
            read_sofar += read_amt;
            // Track `tell` for `Seek`.
            self.told += read_amt as u64;
            // Zero-length reads are expected and should be called on the underlying File.
            if buf.len() == 0 {
                break;
            }
            // Thats all we needed, stop asking for more.
            else if read_sofar >= buf.len() {
                break;
            }
            // Check if the read amount was zero.
            else if read_sofar < buf.len() && read_amt == 0 {
                // If it was zero and more is expected, advance to the next file.
                if !self.advance_file_handle(Direction::Forward)? {
                    break;
                }
            }
        }
        Ok(read_sofar)
    }
}
