use crate::SplitFileReader;

use hex_literal::hex;
use md5::{Digest, Md5};
use std::io::Read;
use std::path::Path;
use std::{fs, io};

fn text_file_paths_parts<'a>() -> Vec<&'a Path> {
    vec![
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.000"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.001"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.002"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.003"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.004"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.005"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.006"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.007"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.008"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.009"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.010"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.011"),
    ]
}

#[test]
fn validate_hashes_text() -> io::Result<()> {
    const BUFFER_SIZE: usize = 256;
    let mut file = fs::File::open("./src/tests/files/plaintext/Adventures_In_Wonderland.txt")?;
    let mut buffer = [0; BUFFER_SIZE];
    let mut hasher = Md5::new();
    loop {
        let read_amt = file.by_ref().read(&mut buffer)?;
        if read_amt == 0 {
            break;
        }
        hasher.update(&buffer[..read_amt]);
    }
    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("7bf855f82dc4af7ee34d65b662bbc58e"));
    Ok(())
}

#[test]
fn simple_read_text() -> io::Result<()> {
    const BUFFER_SIZE: usize = 256;
    let mut sfr = SplitFileReader::new(vec![Path::new(
        "./src/tests/files/plaintext/Adventures_In_Wonderland.txt",
    )])?;
    let mut buffer = [0; BUFFER_SIZE];
    let mut hasher = Md5::new();
    loop {
        let read_amt = sfr.by_ref().read(&mut buffer)?;
        // let mut read_amt = 0;

        if read_amt == 0 {
            break;
        }
        hasher.update(&buffer[..read_amt]);
    }
    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("7bf855f82dc4af7ee34d65b662bbc58e"));
    Ok(())
}

#[test]
fn compound_read_text() -> io::Result<()> {
    const BUFFER_SIZE: usize = 256;
    let mut sfr = SplitFileReader::new(text_file_paths_parts())?;
    let mut buffer = [0; BUFFER_SIZE];
    let mut hasher = Md5::new();
    loop {
        let read_amt = sfr.by_ref().read(&mut buffer)?;
        // let mut read_amt = 0;

        if read_amt == 0 {
            break;
        }
        hasher.update(&buffer[..read_amt]);
    }
    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("7bf855f82dc4af7ee34d65b662bbc58e"));
    Ok(())
}
