use crate::SplitFileReader;

use md5::{Digest, Md5};
use std::io;
use std::io::Read;
use std::path::Path;

#[test]
fn read_to_bad_filename() -> io::Result<()> {
    fn _hash_of_file() -> io::Result<(md5::Md5, usize)> {
        const BUFFER_SIZE: usize = 256;
        let mut file = SplitFileReader::new(vec![
            Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.000"),
            Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.001"),
            Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.00X"),
            Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.003"),
        ])?;
        let mut buffer = [0; BUFFER_SIZE];
        let mut hasher = Md5::new();
        let mut ctr = 0;
        loop {
            let read_amt = file.by_ref().read(&mut buffer)?;
            ctr += read_amt;
            if read_amt == 0 {
                break;
            }
            hasher.update(&buffer[..read_amt]);
        }
        Ok((hasher, ctr))
    }

    let ret = _hash_of_file();
    assert_eq!(ret.unwrap_err().kind(), io::ErrorKind::NotFound);
    Ok(())
}
