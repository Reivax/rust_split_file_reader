mod plaintext_simple {
    /*
    aff4376de65da6550fc8427d13683b7b *files.tar
    4985472dd36295a3013d05298ab4212c *files.tar.000
    60482ee4b68a657b915c2c8e4e244aa9 *files.tar.001
    8ff44ef8e00277b103cc3ba2dae897e2 *files.tar.002
    27e6b0def02cb0c9fc4d73bfb0e2eb42 *files.tar.003
    321d5980ffc4f39d6f0640cd462577df *files.zip
    b16fc3d4464bdedbf41758137041cf60 *files.zip.000
    78a95b9a8e1665a7e6f9bd40aa2ef982 *files.zip.001
    1fe06f3165fafd49cdc717c4f397e045 *files.zip.002
    32839925d10db0b3e46699d2eb8b18e1 *random_payload_1.bin
    7fabb8f1149e9b467429205c9d3d5bcc *random_payload_2.bin
    2b8ccbb2958ba4186e4c842826c32435 *random_payload_3.bin
    a967ed43b9c62312362dd7cb5334bbdb *random_payload_4.bin
    12ce44152c2e5b9a020edbb76cc7ec89 *random_payload_5.bin

     */
    use crate::SplitFileReader;

    use std::io;
    use std::path::Path;
    use tar;

    fn file_paths_parts<'a>() -> Vec<&'a Path> {
        vec![
            Path::new("./src/tests/files/archives/files.tar.000"),
            Path::new("./src/tests/files/archives/files.tar.001"),
            Path::new("./src/tests/files/archives/files.tar.002"),
            Path::new("./src/tests/files/archives/files.tar.003"),
        ]
    }

    #[test]
    fn list_tar_contents() -> io::Result<()> {
        let sfr = SplitFileReader::new(file_paths_parts())?;
        let mut ta = tar::Archive::new(sfr);
        for entry in ta.entries().unwrap() {
            let file = entry.unwrap();
            println!("File: {}", std::str::from_utf8(&file.path_bytes()).unwrap());
        }
        Ok(())
    }
}
