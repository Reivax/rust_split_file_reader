/*
$ cat Adventures_In_Wonderland.txt | md5sum
7bf855f82dc4af7ee34d65b662bbc58e *-

$ cat Adventures_In_Wonderland.txt.* | md5sum
7bf855f82dc4af7ee34d65b662bbc58e *-

$ md5sum *
7bf855f82dc4af7ee34d65b662bbc58e *Adventures_In_Wonderland.txt
99d9cc2ef43215aac8a74ccf2a8d308a *Adventures_In_Wonderland.txt.000
48e1eadb116a6964748c307f8d2bb059 *Adventures_In_Wonderland.txt.001
aef56a3ac176997891f807737725ca58 *Adventures_In_Wonderland.txt.002
fe970d16d8b5f053fa3f09fdba08d422 *Adventures_In_Wonderland.txt.003
eb7e73becdd965a6d5d063d841d02a1d *Adventures_In_Wonderland.txt.004
86a1967a846d11a3e9f50bd1c867b9b3 *Adventures_In_Wonderland.txt.005
4ee296d0f7dd03a704755a812cbe61df *Adventures_In_Wonderland.txt.006
6b138b0bcf7aaf4f592a053a2f485342 *Adventures_In_Wonderland.txt.007
e70fb50e8269fa8e14c988a678d56984 *Adventures_In_Wonderland.txt.008
8848c479069da54b2ba7f6101dfe0841 *Adventures_In_Wonderland.txt.009
4850771bea5b7fca21a98ae24dd381bf *Adventures_In_Wonderland.txt.010
200d31f53d701f9a80cb6dae2387f285 *Adventures_In_Wonderland.txt.011
*/

use crate::SplitFileReader;

use hex_literal::hex;
use md5::{Digest, Md5};
use std::io::{Read, Seek, SeekFrom};
use std::path::Path;
use std::{fs, io};

fn text_file_paths_parts<'a>() -> Vec<&'a Path> {
    vec![
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.000"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.001"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.002"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.003"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.004"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.005"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.006"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.007"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.008"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.009"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.010"),
        Path::new("./src/tests/files/plaintext/Adventures_In_Wonderland.txt.011"),
    ]
}

fn hash_of_file(filepath: &Path) -> io::Result<(md5::Md5, usize)> {
    const BUFFER_SIZE: usize = 256;
    let mut file = fs::File::open(filepath)?;
    let mut buffer = [0; BUFFER_SIZE];
    let mut hasher = Md5::new();
    let mut ctr = 0;
    loop {
        let read_amt = file.by_ref().read(&mut buffer)?;
        ctr += read_amt;
        if read_amt == 0 {
            break;
        }
        hasher.update(&buffer[..read_amt]);
    }
    Ok((hasher, ctr))
}

#[test]
fn validate_hashes_text() -> io::Result<()> {
    let (hasher, size) = hash_of_file(Path::new(
        "./src/tests/files/plaintext/Adventures_In_Wonderland.txt.000",
    ))?;
    let actual_hash = hasher.finalize();
    assert_eq!(actual_hash[..], hex!("99d9cc2ef43215aac8a74ccf2a8d308a"));
    assert_eq!(size, 13962);
    let (hasher, size) = hash_of_file(Path::new(
        "./src/tests/files/plaintext/Adventures_In_Wonderland.txt.001",
    ))?;
    let actual_hash = hasher.finalize();
    assert_eq!(actual_hash[..], hex!("48e1eadb116a6964748c307f8d2bb059"));
    assert_eq!(size, 13962);
    let (hasher, size) = hash_of_file(Path::new(
        "./src/tests/files/plaintext/Adventures_In_Wonderland.txt.002",
    ))?;
    let actual_hash = hasher.finalize();
    assert_eq!(actual_hash[..], hex!("aef56a3ac176997891f807737725ca58"));
    assert_eq!(size, 13962);
    let (hasher, size) = hash_of_file(Path::new(
        "./src/tests/files/plaintext/Adventures_In_Wonderland.txt.003",
    ))?;
    let actual_hash = hasher.finalize();
    assert_eq!(actual_hash[..], hex!("fe970d16d8b5f053fa3f09fdba08d422"));
    assert_eq!(size, 13962);
    Ok(())
}

#[test]
fn compound_seek_text() -> io::Result<()> {
    const FULL_SIZE: i64 = 13962;
    const HALF_SIZE: i64 = FULL_SIZE / 2;
    let mut sfr = SplitFileReader::new(text_file_paths_parts())?;

    let offset = sfr.by_ref().seek(SeekFrom::Start(FULL_SIZE as u64))?;
    assert_eq!(offset, FULL_SIZE as u64 * 1);

    let mut buffer = [0; FULL_SIZE as usize];
    let mut hasher = Md5::new();
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    hasher.update(&buffer[..read_amt]);

    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("48e1eadb116a6964748c307f8d2bb059"));

    let offset = sfr.by_ref().seek(SeekFrom::Current(FULL_SIZE as i64))?;
    assert_eq!(offset, FULL_SIZE as u64 * 3);

    let mut buffer = [0; FULL_SIZE as usize];
    let mut hasher = Md5::new();
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    hasher.update(&buffer[..read_amt]);

    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("fe970d16d8b5f053fa3f09fdba08d422"));

    let offset = sfr.by_ref().seek(SeekFrom::Current(FULL_SIZE as i64))?;
    assert_eq!(offset, FULL_SIZE as u64 * 5);

    let mut buffer = [0; HALF_SIZE as usize];
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    assert_eq!(read_amt, HALF_SIZE as usize);

    let offset = sfr.by_ref().seek(SeekFrom::Current(HALF_SIZE as i64))?;
    assert_eq!(offset, FULL_SIZE as u64 * 6);

    let mut buffer = [0; HALF_SIZE as usize];
    let mut hasher = Md5::new();
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    hasher.update(&buffer[..read_amt]);
    assert_eq!(read_amt, HALF_SIZE as usize);
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    hasher.update(&buffer[..read_amt]);
    assert_eq!(read_amt, HALF_SIZE as usize);

    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("4ee296d0f7dd03a704755a812cbe61df"));

    let offset = sfr.by_ref().seek(SeekFrom::Current(0))?;
    assert_eq!(offset, FULL_SIZE as u64 * 7);

    let offset = sfr
        .by_ref()
        .seek(SeekFrom::Current(FULL_SIZE as i64 * -3))?;
    assert_eq!(offset, FULL_SIZE as u64 * 4);

    let mut buffer = [0; FULL_SIZE as usize];
    let mut hasher = Md5::new();
    let read_amt = sfr.by_ref().read(&mut buffer)?;
    hasher.update(&buffer[..read_amt]);

    let hash = hasher.finalize();
    assert_eq!(hash[..], hex!("eb7e73becdd965a6d5d063d841d02a1d"));

    Ok(())
}

#[test]
fn compound_seek_text_before_start() -> io::Result<()> {
    let mut sfr = SplitFileReader::new(text_file_paths_parts())?;
    sfr.seek(SeekFrom::Current(5))?;
    let _ret = sfr.seek(SeekFrom::Current(-15));
    // let expected = Err(io::ErrorKind::InvalidInput);
    // assert_eq!(ret.map_err(|e| e.kind()), expected);
    assert_eq!(sfr.seek(SeekFrom::Current(0))?, 0);
    Ok(())
}

#[test]
fn compound_seek_text_after_end() -> io::Result<()> {
    let mut sfr = SplitFileReader::new(text_file_paths_parts())?;
    let last_offset = sfr.seek(SeekFrom::End(0))?;
    sfr.seek(SeekFrom::Current(-5))?;
    let _ret = sfr.seek(SeekFrom::Current(15));
    assert_eq!(sfr.seek(SeekFrom::Current(0))?, last_offset);
    Ok(())
}
