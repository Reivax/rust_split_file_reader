mod plaintext_simple {
    /*
    aff4376de65da6550fc8427d13683b7b *files.tar
    4985472dd36295a3013d05298ab4212c *files.tar.000
    60482ee4b68a657b915c2c8e4e244aa9 *files.tar.001
    8ff44ef8e00277b103cc3ba2dae897e2 *files.tar.002
    27e6b0def02cb0c9fc4d73bfb0e2eb42 *files.tar.003
    321d5980ffc4f39d6f0640cd462577df *files.zip
    b16fc3d4464bdedbf41758137041cf60 *files.zip.000
    78a95b9a8e1665a7e6f9bd40aa2ef982 *files.zip.001
    1fe06f3165fafd49cdc717c4f397e045 *files.zip.002
    32839925d10db0b3e46699d2eb8b18e1 *random_payload_1.bin
    7fabb8f1149e9b467429205c9d3d5bcc *random_payload_2.bin
    2b8ccbb2958ba4186e4c842826c32435 *random_payload_3.bin
    a967ed43b9c62312362dd7cb5334bbdb *random_payload_4.bin
    12ce44152c2e5b9a020edbb76cc7ec89 *random_payload_5.bin

     */
    use crate::SplitFileReader;

    use std::io;
    use std::path::Path;

    use hex_literal::hex;
    use md5::{Digest, Md5};
    use std::io::Read;
    use zip::read::ZipArchive;

    fn file_paths_parts<'a>() -> Vec<&'a Path> {
        vec![
            Path::new("./src/tests/files/archives/files.zip.000"),
            Path::new("./src/tests/files/archives/files.zip.001"),
            Path::new("./src/tests/files/archives/files.zip.002"),
        ]
    }

    #[test]
    fn zip_file_names() -> io::Result<()> {
        let sfr = SplitFileReader::new(file_paths_parts())?;
        let mut za = ZipArchive::new(sfr)?;
        assert_eq!(za.len(), 5);
        let zf = za.by_index(0)?;
        assert_eq!(zf.name(), "random_payload_1.bin");
        let zf = za.by_index(1)?;
        assert_eq!(zf.name(), "random_payload_2.bin");
        let zf = za.by_index(2)?;
        assert_eq!(zf.name(), "random_payload_3.bin");
        let zf = za.by_index(3)?;
        assert_eq!(zf.name(), "random_payload_4.bin");
        let zf = za.by_index(4)?;
        assert_eq!(zf.name(), "random_payload_5.bin");
        Ok(())
    }

    #[test]
    fn zip_file_payloads() -> io::Result<()> {
        const BUFFER_SIZE: usize = 256;
        let sfr = SplitFileReader::new(file_paths_parts())?;
        let mut za = ZipArchive::new(sfr)?;
        assert_eq!(za.len(), 5);
        let expected_hashes = vec![
            hex!("32839925d10db0b3e46699d2eb8b18e1"),
            hex!("7fabb8f1149e9b467429205c9d3d5bcc"),
            hex!("2b8ccbb2958ba4186e4c842826c32435"),
            hex!("a967ed43b9c62312362dd7cb5334bbdb"),
            hex!("12ce44152c2e5b9a020edbb76cc7ec89"),
        ];
        for idx in 0..5 {
            let filename = format!("random_payload_{}.bin", idx + 1);
            let mut zf = za.by_name(&*filename)?;

            let mut buffer = [0; BUFFER_SIZE];
            let mut hasher = Md5::new();
            loop {
                let read_amt = zf.by_ref().read(&mut buffer)?;
                if read_amt == 0 {
                    break;
                }
                hasher.update(&buffer[..read_amt]);
            }
            let hash = hasher.finalize();
            assert_eq!(hash[..], expected_hashes[idx]);
        }
        Ok(())
    }

    #[test]
    fn list_zip_contents() -> io::Result<()> {
        let sfr = SplitFileReader::new(file_paths_parts())?;
        let mut za = ZipArchive::new(sfr)?;
        for idx in 0..za.len() {
            println!("{}", za.by_index(idx)?.name());
        }
        Ok(())
    }
}
